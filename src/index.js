const Koa = require('koa');

const app = new Koa();

app.use((ctx) => {
    ctx.body = 'Hello World';
});


//Start the server
app.listen(3000, () => {
    console.log('Server started on localhost:3000');
});